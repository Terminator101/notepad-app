package com.stan.airmiles.notepadapp.notepadapp.notepadappapigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NotepadAppApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotepadAppApiGatewayApplication.class, args);
    }

}

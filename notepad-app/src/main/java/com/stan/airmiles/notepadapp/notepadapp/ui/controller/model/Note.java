package com.stan.airmiles.notepadapp.notepadapp.ui.controller.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Note {

    @NotNull(message = "text field cannot be null")
    @Size(min = 2, message = "text must not be less than two characters")
    private String text;

    @NotNull(message = "userName cannot be null")
    @Email
    private String userName;

    private String city;
}

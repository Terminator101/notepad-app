package com.stan.airmiles.notepadapp.notepadapp.data;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotepadRepository extends CrudRepository<NotepadEntity, Long> {

    List<NotepadEntity> findAll();
    List<NotepadEntity> findByUserName(String email);

}

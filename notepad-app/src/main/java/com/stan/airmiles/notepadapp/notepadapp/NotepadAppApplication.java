package com.stan.airmiles.notepadapp.notepadapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class NotepadAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotepadAppApplication.class, args);
    }

}

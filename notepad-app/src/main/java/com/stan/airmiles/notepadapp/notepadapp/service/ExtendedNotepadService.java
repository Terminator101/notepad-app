package com.stan.airmiles.notepadapp.notepadapp.service;

import com.stan.airmiles.notepadapp.notepadapp.data.NotepadEntity;
import com.stan.airmiles.notepadapp.notepadapp.data.NotepadRepository;
import com.stan.airmiles.notepadapp.notepadapp.dto.NoteDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ExtendedNotepadService implements NotepadService {

    @Autowired
    NotepadRepository notepadRepository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public NoteDto saveNote(NoteDto noteDto) {

        NotepadEntity notepadEntity =  modelMapper.map(noteDto, NotepadEntity.class);
        notepadRepository.save(notepadEntity);

        return noteDto;
    }

    @Override
    public List<NoteDto> getAllNotes() {

        List<NoteDto> result = new ArrayList<>();
        result = Arrays.asList(modelMapper.map(notepadRepository.findAll(), NoteDto[].class));
        return result;
    }

    @Override
    public List<NoteDto> getNotesByUsername(String email) {
        List<NoteDto> result = Arrays.asList(modelMapper.map(notepadRepository.findByUserName(email), NoteDto[].class));
        return result;
    }
}

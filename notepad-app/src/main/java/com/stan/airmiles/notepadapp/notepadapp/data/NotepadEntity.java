package com.stan.airmiles.notepadapp.notepadapp.data;

import com.sun.istack.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Table(name = "notes")
@AllArgsConstructor
@NoArgsConstructor
public class NotepadEntity {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 120)
    private String text;

    @Column(nullable = false)
    private String userName;

    private String city;
}

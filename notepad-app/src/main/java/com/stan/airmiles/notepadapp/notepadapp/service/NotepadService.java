package com.stan.airmiles.notepadapp.notepadapp.service;

import com.stan.airmiles.notepadapp.notepadapp.dto.NoteDto;

import java.util.List;


public interface NotepadService  {

    NoteDto saveNote(NoteDto noteDto);
    List<NoteDto> getAllNotes();
    List<NoteDto> getNotesByUsername(String email);
}

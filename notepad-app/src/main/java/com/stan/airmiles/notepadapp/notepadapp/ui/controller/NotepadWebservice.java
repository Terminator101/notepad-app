package com.stan.airmiles.notepadapp.notepadapp.ui.controller;

import com.stan.airmiles.notepadapp.notepadapp.dto.NoteDto;
import com.stan.airmiles.notepadapp.notepadapp.service.NotepadService;
import com.stan.airmiles.notepadapp.notepadapp.ui.controller.model.Note;
import com.stan.airmiles.notepadapp.notepadapp.ui.controller.model.NoteResponseModel;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.modelmapper.spi.MatchingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/api/notes")
public class NotepadWebservice {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private NotepadService notepadService;

    @GetMapping("/all")
    public List<NoteResponseModel> getAllNotes() {
        List<NoteResponseModel> result = new ArrayList<>();
        result = Arrays.asList(modelMapper.map(notepadService.getAllNotes(), NoteResponseModel[].class));
        return result;
    }

    @GetMapping(path = "/{email}")
    public List<NoteResponseModel> getNoteByUser(@PathVariable String email) {
        List<NoteResponseModel> result = new ArrayList<>();
        result = Arrays.asList(modelMapper.map(notepadService.getNotesByUsername(email), NoteResponseModel[].class));
        return result;
    }

    @PostMapping(
            path = "/create",
            consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<NoteResponseModel> saveText(@Valid @RequestBody Note note) {

        NoteDto noteDto = modelMapper.map(note, NoteDto.class);
        notepadService.saveNote(noteDto);

        NoteResponseModel noteResponseModel = modelMapper.map(noteDto, NoteResponseModel.class);

        return ResponseEntity.status(HttpStatus.OK).body(noteResponseModel);
    }
}

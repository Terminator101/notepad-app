import React, {Component, useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import NotesPage from './pages/notes/notes.page';
import {BrowserRouter as Router, Route, Link, Switch, Redirect} from 'react-router-dom';

export default class App extends Component {
    
  constructor(props) {
    super(props);
  }

    render() {
      return (
        <div className='App'>
          <Router>
                <Switch>
                <Route exact path="/" exact component={NotesPage} />
                </Switch>
          </Router>
        </div>
      );
    }
}
 

import React,{useState, useEffect} from 'react';
import { Component } from 'react';
//import {Note} from '../../models/note';
import axios from 'axios';
import NotepadService from '../../services/notepad.service'


export default function NotesPage(props) {

    const[note, setNote] = useState({
        text: '',
        userName: '',
        city: ''
    });

    const API_URL = "http://ec2-3-96-55-54.ca-central-1.compute.amazonaws.com:8011/notepad-ms/api/notes/create";

    function submit(e) {
        alert('Your text was submitted: ' +this.note)
        e.preventDefault();
        const headers = {
            'Content-Type' : 'application/json'
        }
        axios.post(API_URL, JSON.stringify({
            note
        }), {
            headers: headers
        })  .then(res => {
                    console.log(res.note);
                })
        
    }
    
    function handle(e) {
        setNote({...note, [e.target.userName]: e.target.value });
        console.log(e.target.value);
    }

    return (
    <div className="container">
        <h1>Notes App</h1>
        <form onSubmit={(e) => submit(e)}>
        <div className="form-group">
                <input type='text' name="userName" id="userName" defaultValue="email" placeholder="Enter a valid Email as UserName" className="form-control" onChange={(e)=>handle(e)}/>
            </div>
            <div className="form-group">
                <input type='text' name="city" id="city" placeholder="city" className="form-control" defaultValue="city" onChange={(e)=>handle(e)}/>
            </div>
            <div className="form-group">
                <input type='text' name="text" id="text" placeholder="Enter your notes here" className="form-control" defaultValue="text" onChange={(e)=>handle(e)}/>
            </div>
            <button className="btn btn-primary">Done</button>
        </form>
    </div>
    );
    
}
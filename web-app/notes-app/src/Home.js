import React, {Component, useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import NotesPage from './pages/notes/notes.page';
import {BrowserRouter as Router, Route, Link, Switch, Redirect} from 'react-router-dom';
import MainPage from './component/MainPage';
import PostForm from './component/PostForm';
import UserLoginForm from './component/UserLoginForm';
import App from './App';

export default class Home extends Component {
    
  constructor(props) {
    super(props);

    this.handleSignin = this.handleSignin.bind(this);

  }

  handleSignin() {
    this.props.history.push("/notes");
  }

    render() {
      return (
        <div className='App'>
          <UserLoginForm handleSignin={this.handleSignin}/>
        </div>
      );
    }
}
 

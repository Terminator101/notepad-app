import http from "../http-common"

export default class NotepadService {

    getAll() {
        return http.get("/notes");
    }

    create(data) {
        return http.post("/notes", data);
    }
}
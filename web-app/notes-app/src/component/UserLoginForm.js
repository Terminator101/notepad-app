import React from 'react';

export default class UserLoginForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            userName: '',
            city: '',
            loginErrors: ""
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

     handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }


     handleSubmit(e) {
        e.preventDefault();
      const {userName, city} = this.state;
      console.log(userName);
      console.log(city);
    }

    render() {
      return (
            <div className="container">
            <h1>Notes App</h1>
            <form onSubmit={(e) => this.handleSubmit(e)}>
            <div className="form-group">
                    <input type='text' name="userName" id="userName" placeholder="Enter a valid Email as UserName" className="form-control" onChange={(e)=>this.handleChange(e)}/>
                </div>
                <div className="form-group">
                    <input type='text' name="city" id="city" placeholder="city" className="form-control" onChange={(e)=>this.handleChange(e)}/>
                </div>
                <button className="btn btn-primary">Login</button>
            </form>
        </div>
        )
    }
}

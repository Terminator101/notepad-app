package com.stan.airmiles.notepadapp.notepadapp.notepadappdiscoveryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class NotepadAppDiscoveryServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotepadAppDiscoveryServiceApplication.class, args);
    }

}

package com.stan.microservice.photoapp.users.photoappusersms.ui.controller;

import com.stan.microservice.photoapp.users.photoappusersms.dto.UserDto;
import com.stan.microservice.photoapp.users.photoappusersms.service.UserService;
import com.stan.microservice.photoapp.users.photoappusersms.ui.model.CreateUserResponseModel;
import com.stan.microservice.photoapp.users.photoappusersms.ui.model.User;
import com.stan.microservice.photoapp.users.photoappusersms.ui.model.UserResponseModel;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private Environment environment;

    @Autowired
    private UserService userService;

    @GetMapping("/status/check")
    public String status() {
        return "Working on port "+ environment.getProperty("local.server.port");
    }

    @PostMapping(
            consumes = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<CreateUserResponseModel> createUser(@Valid @RequestBody User userDetails) {

        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);

        UserDto userDto = mapper.map(userDetails, UserDto.class);
        userService.createUser(userDto);

        CreateUserResponseModel createUserResponseModel = mapper.map(userDto, CreateUserResponseModel.class);

        return  ResponseEntity.status(HttpStatus.CREATED).body(createUserResponseModel);
    }

    @GetMapping(value = "/{userId}", produces = {MediaType.APPLICATION_JSON_VALUE,
                                                 MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<UserResponseModel> getUser(@PathVariable("userId") String userId) {

            UserDto userDto = userService.getUserById(userId);
        UserResponseModel returnValue = new ModelMapper().map(userDto, UserResponseModel.class);

        return ResponseEntity.status(HttpStatus.OK).body(returnValue);
    }
}

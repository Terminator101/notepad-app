package com.stan.microservice.photoapp.users.photoappusersms.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stan.microservice.photoapp.users.photoappusersms.dto.UserDto;
import com.stan.microservice.photoapp.users.photoappusersms.service.UserService;
import com.stan.microservice.photoapp.users.photoappusersms.ui.model.LoginRequestModel;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private UserService userService;

    private Environment environment;

    public AuthenticationFilter(Environment environment, UserService userService, AuthenticationManager authenticationManager) {
        this.environment = environment;
        this.userService = userService;
        super.setAuthenticationManager(authenticationManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {

           LoginRequestModel creds =  new ObjectMapper()
                    .readValue(request.getInputStream(), LoginRequestModel.class);

           return getAuthenticationManager().authenticate(
                   new UsernamePasswordAuthenticationToken(
                           creds.getEmail(),
                           creds.getPassword(),
                           new ArrayList<>())
           );
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        // From the request getting the email id which is our userName
        String userName = ((User) authResult.getPrincipal()).getUsername();

        UserDto userDetail = userService.getUserDetailsByEmail(userName);

        String token = Jwts.builder()
                            .setSubject(userDetail.getUserId())
                            .setExpiration(new Date(System.currentTimeMillis() + Long.parseLong(
                                                                      environment.getProperty("token.expiration_time"))))
                .signWith(SignatureAlgorithm.HS512, environment.getProperty("token.secret"))
                .compact();

        response.addHeader("token", token);
        response.addHeader("userId", userDetail.getUserId());
    }
}

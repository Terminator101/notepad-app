package com.stan.microservice.photoapp.users.photoappusersms.ui.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequestModel {

    private String email;
    private String password;

}

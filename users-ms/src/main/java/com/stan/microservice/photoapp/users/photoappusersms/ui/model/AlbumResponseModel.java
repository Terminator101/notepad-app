package com.stan.microservice.photoapp.users.photoappusersms.ui.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AlbumResponseModel {

    private String albumId;

    private String userId;

    private String name;

    private String description;
}

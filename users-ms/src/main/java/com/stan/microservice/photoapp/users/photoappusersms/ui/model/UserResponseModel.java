package com.stan.microservice.photoapp.users.photoappusersms.ui.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserResponseModel {

    private String userId;

    private String firstName;

    private String lastName;

    private List<AlbumResponseModel> albums;

}

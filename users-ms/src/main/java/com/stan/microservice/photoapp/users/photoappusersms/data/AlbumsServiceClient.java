package com.stan.microservice.photoapp.users.photoappusersms.data;

import com.stan.microservice.photoapp.users.photoappusersms.ui.model.AlbumResponseModel;
import feign.FeignException;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import java.util.List;

@FeignClient(name = "albums-ws", fallbackFactory =AlbumsFallbackFactory.class)
public interface AlbumsServiceClient {

    @GetMapping("/users/{id}/albums")
    public List<AlbumResponseModel> getAlbum(@PathVariable String id);
}

@Component
class AlbumsFallbackFactory implements FallbackFactory<AlbumsServiceClient> {

    @Override
    public AlbumsServiceClient create(Throwable cause) {
        return new AlbumsServiceClientFallback(cause);
    }
}

class AlbumsServiceClientFallback implements AlbumsServiceClient {

    Logger log = LoggerFactory.getLogger(this.getClass());

    private final Throwable cause;

   public AlbumsServiceClientFallback(Throwable cause) {
       this.cause = cause;
    }

    @Override
    public List<AlbumResponseModel> getAlbum(String id) {

       if(cause instanceof FeignException && ((FeignException) cause).status() == 404) {
           log.error("404 error happened when requested for albums with userId: "
           + id +".Error Message: "
           +cause.getLocalizedMessage());
       } else {
           log.error("Errorr occurred while requesting albums using userId: " +cause.getLocalizedMessage());
       }
        return null;
    }
}
package com.stan.microservice.photoapp.users.photoappusersms.dto;

import com.stan.microservice.photoapp.users.photoappusersms.ui.model.AlbumResponseModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {

    private String userId;

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private String encryptedPassword;

    private List<AlbumResponseModel> albums;
}

package com.stan.microservice.photoapp.users.photoappusersms.service;

import com.stan.microservice.photoapp.users.photoappusersms.dto.UserDto;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Service
public interface UserService extends UserDetailsService {

    UserDto createUser(UserDto userDetails);
    UserDto getUserDetailsByEmail(String email);
    UserDto getUserById(String id);
}

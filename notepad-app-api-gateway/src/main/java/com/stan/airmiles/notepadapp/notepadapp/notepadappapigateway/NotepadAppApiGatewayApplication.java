package com.stan.airmiles.notepadapp.notepadapp.notepadappapigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class NotepadAppApiGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotepadAppApiGatewayApplication.class, args);
    }

}
